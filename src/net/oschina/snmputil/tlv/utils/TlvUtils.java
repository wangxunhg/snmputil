﻿package net.oschina.snmputil.tlv.utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * BER编码一种，ASN.1标准，全称Type（类型），Length（长度），Value（值）。
IS-IS数据通信领域中，tlv三元组： Type-length-value（TLV）。T、L字段的长度往往固定（通常为1～4bytes），
V字段长度可变。顾名思义，T字段表示报文类型，L字段表示报文长度、V字段往往用来存放报文的内容。
 * @author zdc
 * @since 1.0
 */
public class TlvUtils {

	/**
	 * 
	 * 
	 * @param hexString
	 * @return
	 */
	public static List<Tlv> builderTlvList(String hexString) {
		List<Tlv> tlvs = new ArrayList<Tlv>();

		int position = 0;
		while (position != StringUtils.length(hexString)) {
			String _hexTag = getTag(hexString, position);
			position += _hexTag.length();

			LPositon l_position = getLengthAndPosition(hexString, position);
			int _vl = l_position.get_vL();

			position = l_position.get_position();

			String _value = StringUtils.substring(hexString, position, position
					+ _vl * 2);

			position = position + _value.length();

			tlvs.add(new Tlv(_hexTag, _vl, _value));
		}
		return tlvs;
	}

	/**
	 * 
	 * 
	 * @param hexString 十六进制字符串 比如:"012345678ABC"
	 * @return
	 */
	public static Map<String, Tlv> builderTlvMap(String hexString) {

		Map<String, Tlv> tlvs = new HashMap<String, Tlv>();

		int position = 0;
		System.out.println(hexString.length());
		while (position != hexString.length()) {
			String _hexTag = getTag(hexString, position);
			//System.out.println(_hexTag);
			position += _hexTag.length();

			//System.out.println(position);
			LPositon l_position = getLengthAndPosition(hexString, position);

			int _vl = l_position.get_vL();
			position = l_position.get_position();
			String _value = hexString.substring(position, position + _vl * 2);
			position = position + _value.length();

			tlvs.put(_hexTag, new Tlv(_hexTag, _vl, _value));
		}
		return tlvs;
	}

	/**
	 * 
	 * 
	 * @param hexString
	 * @param position
	 * @return
	 */
	private static LPositon getLengthAndPosition(String hexString, int position) {
		String firstByteString = hexString.substring(position, position + 2);
		int i = Integer.parseInt(firstByteString, 16);
		String hexLength = "";

		if (((i >>> 7) & 1) == 0) {
			hexLength = hexString.substring(position, position + 2);
			position = position + 2;
		} else {

			int _L_Len = i & 127;
			position = position + 2;
			hexLength = hexString.substring(position, position + _L_Len * 2);

			position = position + _L_Len * 2;
		}
		return new LPositon(Integer.parseInt(hexLength, 16), position);

	}

	/**
	 *
	 * 
	 * @param hexString
	 * @param position
	 * @return
	 */
	private static String getTag(String hexString, int position) {
		String firstByte = StringUtils.substring(hexString, position,
				position + 2);
		// System.out.println(firstByte);
		int i = Integer.parseInt(firstByte, 16);
		if ((i & 0x1f) == 0x1f) {
			return hexString.substring(position, position + 4);

		} else {
			return hexString.substring(position, position + 2);
		}
	}

	
}