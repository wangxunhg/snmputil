package net.oschina.snmputil.snmp;

/**
 * 
 * @author zdc
 *
 */
public abstract class AbstractSnmpService implements SnmpService {

	protected boolean isUseUdp;
	protected String host;
	protected int port;
	protected String writeCommunity;
	protected String readCommunity;
	protected int version;
	protected int retries;
	protected long timeout;

	AbstractSnmpService() {
		isUseUdp = true;
		retries = 3;
		timeout = 5000;
	}

	public void setUseTcp() {
		isUseUdp = false;
	}

	@Override
	public void setCommunity(int version, String writeCommity,
			String readCommity) {
		this.version = version;
		this.writeCommunity = writeCommity;
		this.readCommunity = readCommity;
	}

	@Override
	public void setRetries(int retries) {
		this.retries = retries;
	}

	@Override
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
}
