package net.oschina.snmputil.iputils;

/**
 * 
 * @author zdc
 *
 */
public class ConversionUtil {
	/***
	 * 
	 * 将二进制字符串转化为10进制
	 * @param hex
	 * @return
	 */
	public static int hexStringToAlgorism(String hex) {
		hex = hex.toUpperCase();
		int max = hex.length();
		int result = 0;
		for (int i = max; i > 0; i--) {
			char c = hex.charAt(i - 1);
			int algorism = 0;
			if (c >= '0' && c <= '9') {
				algorism = c - '0';
			} else {
				algorism = c - 55;
			}
			result += Math.pow(16, max - i) * algorism;
		}
		return result;
	}


}
