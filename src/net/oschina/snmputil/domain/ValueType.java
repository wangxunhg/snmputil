package net.oschina.snmputil.domain;
/**
 *  snmp 数据类型
 * */
public enum ValueType {
	VT_SPSTRING, VT_INTEGER, VT_UNSIGNED, VT_STRING, VT_LONG, VT_HEXSTRING, VT_DECIMALSTRING, VT_NULLOBJ, VT_OBJID, VT_TIMETICKS, VT_ADDRESS, VT_BITS, VT_OTHER, VT_DATE, VT_IP, VT_GAUGE32
}
