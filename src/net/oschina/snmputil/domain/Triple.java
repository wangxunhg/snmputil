package net.oschina.snmputil.domain;
import java.io.Serializable;
/**
 * SNMPset数据三元组
 * @author zdc
 *
 * @param <K>
 * @param <V>
 * @param <T>
 */
public class Triple<K extends Object, V extends Object, T extends Object> implements Serializable {

	private static final long serialVersionUID = 1044571607914104542L;

	private K first;
	private V second;
	private T third;

	public Triple(K k, V v, T t) {
		first = k;
		second = v;
		third = t;
	}

	public K getFirst() {
		return first;
	}

	public V getSecond() {
		return second;
	}

	public void setThird(T third) {
		this.third = third;
	}

	public T getThird() {
		return third;
	}
}